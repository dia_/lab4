package com.example.lab4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.util.Pair;
import android.view.WindowManager;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


interface Object {
    public void Draw(GL10 gl10);
}

class Vec {
    private float mx,my,mz;

    //конструктор, задаем значения
    Vec(float x, float y, float z) {
        mx = x;
        my = y;
        mz = z;
    }
    Vec(Vec vector) {
        mx = vector.X();
        my = vector.Y();
        mz = vector.Z();
    }

    //установка и получение
    public float X() {
        return mx;
    }
    public Vec X(float val) {
        mx = val;
        return this;
    }
    public float Y() {
        return my;
    }
    public Vec Y(float val) {
        my = val;
        return this;
    }
    public float Z() {
        return mz;
    }
    public Vec Z(float val) {
        mz = val;
        return this;
    }

    public Vec Clone() {
        return new Vec(this);
    }

    public float Length() {
        return ((float) Math.sqrt(this.X() * this.X() + this.Y() * this.Y() + this.Z() * this.Z()));
    }

    public Vec Normalize() {
        float length = this.Length();
        return (length == 0) ? this : this.Scale(1 / length, this);
    }
    public Vec Add(Vec vector) {
        this.X(this.X() + vector.X());
        this.Y(this.Y() + vector.Y());
        this.Z(this.Z() + vector.Z());
        return this;
    }
    public Vec Sub(Vec vector) {
        this.X(this.X() - vector.X());
        this.Y(this.Y() - vector.Y());
        this.Z(this.Z() - vector.Z());
        return this;
    }
    public Vec M(float value) {
        this.X(this.X() * value);
        this.Y(this.Y() * value);
        this.Z(this.Z() * value);
        return this;
    }
    public Vec M(float value, Vec out) {
        out.X(this.X() * value);
        out.Y(this.Y() * value);
        out.Z(this.Z() * value);
        return out;
    }
    public Vec Scale(float value) {
        this.X(this.X() * value);
        this.Y(this.Y() * value);
        this.Z(this.Z() * value);
        return this;
    }
    public Vec Scale(float value, Vec out) {
        out.X(this.X() * value);
        out.Y(this.Y() * value);
        out.Z(this.Z() * value);
        return out;
    }
    public float Dot(Vec vector) {
        return this.X() * vector.X() + this.Y() * vector.Y() + this.Z() * vector.Z();
    }
    public Vec Reflect(Vec vector) {
        return this.Sub(vector.M((float) (2.0 * vector.Dot(this))));
    }
}

//Сетке нужен набор вершин и индексы
class Mesh {
    public ArrayList<Vec> vertices = new ArrayList<>();
    public ArrayList<Integer> indices = new ArrayList<>();

    private int GetNewVertex(int i1, int i2, ArrayList<Pair<Pair<Integer, Integer>, Integer>> newVertices) {
        //кортежи для индексов
        Pair<Integer, Integer> t1 = new Pair<>(i1, i2);
        Pair<Integer, Integer> t2 = new Pair<>(i2, i1);

        for (Pair<Pair<Integer, Integer>, Integer> tuple : newVertices) {
            if (tuple.first.first == t2.first && tuple.first.second == t2.second)
                return tuple.second;
        }

        for (Pair<Pair<Integer, Integer>, Integer> tuple : newVertices) {
            if (tuple.first.first == t1.first && tuple.first.second == t1.second)
                return tuple.second;
        }
        int newIndex = this.vertices.size();
        this.vertices.add(new Vec(this.vertices.get(i1).Clone().Add(this.vertices.get(i2)).Scale(0.5f)));
        return newIndex;
    }

    public void Sub() {
        ArrayList<Pair<Pair<Integer, Integer>, Integer>> newVertices = new ArrayList<>();
        ArrayList<Integer> newIndices = new ArrayList<>();
        int newIndicesCount = 0;
        for (int index = 0; index < this.indices.size() / 3; index++) {
            int in1 = this.indices.get(index * 3);
            int in2 = this.indices.get(index * 3 + 1);
            int in3 = this.indices.get(index * 3 + 2);
            int v1 = this.GetNewVertex(in1, in2, newVertices);
            int v2 = this.GetNewVertex(in2, in3, newVertices);
            int v3 = this.GetNewVertex(in3, in1, newVertices);
            ArrayList<Integer> IndicesRange = new ArrayList<Integer>(Arrays.asList(in1, v1, v3, in2, v2, v1, in3, v3, v2, v1, v2, v3));
            for (int i = 0; i < 12; i++) {
                newIndices.add(newIndicesCount + i, IndicesRange.get(i));
            }
            newIndicesCount += 12;
        }
        this.indices = newIndices;
    }
}

class SphereAndLight implements Object
{   //12 вершин многогранника
    ArrayList<Vec> figureVertices = new ArrayList<Vec>(Arrays.asList(
            new Vec(-0.3f, 0f, 0.5f), new Vec(0.3f, 0f, 0.5f),
            new Vec(-0.3f, 0f, -0.5f), new Vec(0.3f, 0f, -0.5f),
            new Vec(0f, 0.5f, 0.3f), new Vec(0f, 0.5f, -0.3f),
            new Vec(0f, -0.5f, 0.3f), new Vec(0f, -0.5f, -0.3f),
            new Vec(0.5f, 0.3f, 0f), new Vec(-0.5f, 0.3f, 0f),
            new Vec(0.5f, -0.3f, 0f), new Vec(-0.5f, -0.3f, 0f)
    ));
    //20 граней
    ArrayList<Integer> figureIndices = new ArrayList<Integer>(Arrays.asList(
            1,4,0,4,9,0,4,5,9,8,5,4,1,8,4,1,10,8,10,3,8,8,3,5,3,2,5,3,7,2,
            3,10,7,10,6,7,6,11,7,6,0,11,6,1,0,10,1,6,11,0,9,2,11,9,5,2,9,11,2,7
    ));

    private FloatBuffer vertexCFBuffer;
    private FloatBuffer vertexColFBuffer;
    private ShortBuffer vertexOSBuffer;

    private int elCount = 0;

    //создание буфера координат вершин
    private FloatBuffer createVertexCBuffer(float[] vertexCoordinates) {
        ByteBuffer vertexCoordinatesByteBuffer = ByteBuffer.allocateDirect(Float.BYTES * vertexCoordinates.length);
        vertexCoordinatesByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer vertexCoordinatesFloatBuffer = vertexCoordinatesByteBuffer.asFloatBuffer();
        vertexCoordinatesFloatBuffer.put(vertexCoordinates);
        vertexCoordinatesFloatBuffer.position(0);
        return vertexCoordinatesFloatBuffer;
    }
    //создание буфера цвета вершин
    private FloatBuffer createVertexColBuffer(float[] vertexColors) {
        ByteBuffer vertexColorsByteBuffer = ByteBuffer.allocateDirect(Float.BYTES * vertexColors.length);
        vertexColorsByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer vertexColorsFloatBuffer = vertexColorsByteBuffer.asFloatBuffer();
        vertexColorsFloatBuffer.put(vertexColors);
        vertexColorsFloatBuffer.position(0);
        return vertexColorsFloatBuffer;
    }
    //порядка вершин
    private ShortBuffer createVertexOBuffer(short[] vertexOrder) {
        ByteBuffer vertexOrderByteBuffer = ByteBuffer.allocateDirect(Short.BYTES * vertexOrder.length);
        vertexOrderByteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer vertexOrderShortBuffer = vertexOrderByteBuffer.asShortBuffer();
        vertexOrderShortBuffer.put(vertexOrder);
        vertexOrderShortBuffer.position(0);
        return vertexOrderShortBuffer;
    }

    //параметры - радиус фигуры, рассеяние, цвет рассеянного освещения, цвет отраженного освещения ,позиция света, фигура)
    SphereAndLight(float radius, int s, float[] diffColor, float[] specColor, Vec light, Vec view) {
        Mesh mesh = new Mesh();
        mesh.vertices.addAll(figureVertices);
        mesh.indices.addAll(figureIndices);

        for (int i = 0; i < s; i++) {
            mesh.Sub();
        }

        for (Vec vector: mesh.vertices) {
            vector.Normalize().Scale(radius);
        }

        float[] vertexCoordinates= new float[mesh.vertices.size() * 3];
        for (int i = 0; i < mesh.vertices.size(); i++) {
            vertexCoordinates[i * 3] = mesh.vertices.get(i).X();
            vertexCoordinates[i * 3 + 1] = mesh.vertices.get(i).Y();
            vertexCoordinates[i * 3 + 2] = mesh.vertices.get(i).Z();
        }

        float specPower = 3f;

        light.Normalize();
        view.Normalize();

        float[] vertexColors = new float[mesh.vertices.size() * 4];
        for (int i = 0; i < mesh.vertices.size(); i++) {
            Vec vertex = new Vec(mesh.vertices.get(i).X(), mesh.vertices.get(i).Y(), mesh.vertices.get(i).Z()).Normalize();

            Vec reflect = new Vec(0, 0, 0);

            view.M(-1, reflect).Reflect(vertex.Clone()).Normalize();
            //установка точек рассеянного света
            float diff = (float) Math.max(vertex.Dot(light), .0f);
            //отраженного света
            float spec = (float) Math.pow(Math.max(light.Dot(reflect), .0f), specPower);

            //переходы света
            vertexColors[i * 4] = diffColor[0] * diff + specColor[0] * spec;
            vertexColors[i * 4 + 1] = diffColor[1] * diff + specColor[1] * spec;
            vertexColors[i * 4 + 2] = diffColor[2] * diff + specColor[2] * spec;
            vertexColors[i * 4 + 3] = diffColor[3] + specColor[3];
        }

        short[] vertexOrder = new short[mesh.indices.size()];
        for (int i = 0; i < mesh.indices.size(); i++) {
            vertexOrder[i] = ((short)(int) mesh.indices.get(i));
        }

        vertexCFBuffer = this.createVertexCBuffer(vertexCoordinates);
        vertexColFBuffer = this.createVertexColBuffer(vertexColors);
        vertexOSBuffer = this.createVertexOBuffer(vertexOrder);

        elCount = vertexOrder.length;
    }

    /**
     * метод отрисовки объекта
     */
    @Override
    public void Draw(GL10 gl10) {
        //поддержка массивов вершин
        gl10.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        //записываем размер, тип, и сам буфер с точками
        gl10.glVertexPointer(3, GL10.GL_FLOAT, GL10.GL_ZERO, this.vertexCFBuffer);
        //поддержка массива цветов
        gl10.glEnableClientState(GL10.GL_COLOR_ARRAY);
        //Задает массив цвета вершин
        gl10.glColorPointer(4, GL10.GL_FLOAT, GL10.GL_ZERO, this.vertexColFBuffer);
        //Отображает указанные вершины из массива по индексам
        gl10.glDrawElements(GL10.GL_TRIANGLES, this.elCount, GL10.GL_UNSIGNED_SHORT, this.vertexOSBuffer);
    }
}

class MyRenderer implements GLSurfaceView.Renderer {
    Activity activ;
    Object myObject;
    int counter = 0;

    public MyRenderer(Activity activity) {
        activ = activity;
    }
    /**
     * метод вызывается при создании поверхности
     */
    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        myObject = new SphereAndLight(1f, 2, new float[] {0f, 0.8f, 0f, 1f,},
                new float[] {0f, 0f, .9f, 1f,}, new Vec(0, 1, -1), new Vec(0, 0, -1)
        );
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int i, int i1) {

    }
    /**
     * метод вызывается для рисования текущего кадра
     */
    @Override
    public void onDrawFrame(GL10 gl10) {
        gl10.glClearColor(0,0,0,0);
        gl10.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        //загружает матрицу преобразования
        gl10.glLoadIdentity();
        //масштаб объекта
        gl10.glScalef(0.4f, 0.3f, 0.4f);

        gl10.glEnable(GL10.GL_DEPTH_TEST);
        gl10.glDepthFunc(GL10.GL_LESS);
        gl10.glEnable(GL10.GL_BLEND);

        myObject.Draw(gl10);

        gl10.glDisableClientState(GL10.GL_COLOR_ARRAY);
        gl10.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }
}
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        GLSurfaceView g = new GLSurfaceView(this);
        g.setRenderer(new MyRenderer(this));
        g.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        setContentView(g);
    }
}